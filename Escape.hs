module Escape where

escape :: [(Char, String)] -> String -> String
escape = foldr f id
  where f (c,s) acc = acc . replaceOne (c,s)
        replaceOne (c,s) = concatMap (\x -> if x == c then s else [x])
