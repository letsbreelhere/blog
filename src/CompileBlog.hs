module Main where

import           Control.Lens (view)
import           Control.Monad (forM_, forM, foldM, when)
import           Data.Blog
import           Data.Post
import           Data.Text (Text, pack)
import qualified Data.Text.Lazy.IO as T
import           System.Directory
import           System.FilePath.Posix
import           System.FilePath.Posix.Lens
import           Text.Layout
import qualified Text.Blaze.Html.Renderer.Text as Blaze

partitionM :: Monad m => (a -> m Bool) -> [a] -> m ([a],[a])
partitionM p = flip foldM ([],[]) $ \(t,f) x -> do
  satisfied <- p x
  return $ if satisfied then (x:t,f) else (t,x:f)

getRecursiveContents :: FilePath -> IO [FilePath]
getRecursiveContents path = do
 directoryContents <- map (combine path) . filter (`notElem` [".", ".."]) <$> getDirectoryContents path
 (directories, files) <- partitionM doesDirectoryExist directoryContents
 rest <- concat <$> mapM getRecursiveContents directories
 return $ files ++ rest

getPosts :: FilePath -> IO [Post]
getPosts rootPath = do
  postPaths <- getRecursiveContents rootPath
  forM postPaths $ \path -> either (error . show) return =<< parsePostFromFile path path

main :: IO ()
main = do
  let compiledDir = "_posts"
  shouldKillDirectory <- doesDirectoryExist compiledDir
  when shouldKillDirectory $ removeDirectoryRecursive compiledDir
  posts <- getPosts "posts"
  let blog = Blog "Bree Elle's Blog" "Bree Elle Stanwyck"

  writeHtml "index.html" $ renderIndex blog posts

  forM_ posts $ \post -> do
    createDirectoryIfMissing True (view _directory $ outPath post)
    writeHtml (outPath post) $ renderPost blog post
  where writeHtml path = T.writeFile path . Blaze.renderHtml
