module Text.Layout where

import           Data.Blog (Blog)
import qualified Data.Blog as Blog
import           Data.Post (Post)
import qualified Data.Post as Post
import           Data.String (fromString)
import           Data.Text
import           Text.Blaze.Html5 hiding (title, body)
import qualified Text.Blaze.Html5 as H
import           Text.Blaze.Html5.Attributes hiding (id, div, title)
import qualified Text.Blaze.Html5.Attributes as Attr

renderLayout :: Blog -> Text -> Html -> Html
renderLayout blog title content = do
  docType
  html $ do
    H.head . H.title . toHtml $ title
    H.body $ do
      h2 ! Attr.id "header" $ a ! href "/" $ toHtml $ Blog.title blog
      H.div ! class_ "content" $ content

renderPost :: Blog -> Post -> Html
renderPost blog post =
  let content = Post.body post
      title   = Post.title . Post.frontMatter $ post
      author  = Post.author . Post.frontMatter $ post
  in renderLayout blog title content

renderIndex :: Blog -> [Post] -> Html
renderIndex blog posts =
  let postHtmls = fmap renderIndexPost posts
      title = Blog.title blog
  in renderLayout blog title (sequence_ postHtmls)

renderIndexPost :: Post -> Html
renderIndexPost post =
  let content = Post.body post
      title   = Post.title . Post.frontMatter $ post
      author  = Post.author . Post.frontMatter $ post
  in H.div ! class_ "article" $ do
       h1 $ a ! href (fromString $ Post.outPath post) $ toHtml title
       content
