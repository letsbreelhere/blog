module System.FilePath.Posix.Lens where

import Control.Lens (Lens', lens)
import System.FilePath.Posix

_extension :: Lens' FilePath String
_extension = lens takeExtension replaceExtension

_directories :: Lens' FilePath [FilePath]
_directories = lens splitDirectories (const joinPath)

_directory :: Lens' FilePath String
_directory = lens takeDirectory replaceDirectory
