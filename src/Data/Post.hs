module Data.Post where

import           Control.Lens (_head, set, view, (&))
import           Data.Maybe (fromMaybe)
import           Data.Text (Text, pack)
import qualified Data.Text.IO as T
import           Text.Parsec
import           Text.Parsec.Text (Parser)
import           Text.Sundown.Html.Text (noExtensions, noHtmlModes)
import qualified Text.Sundown.Html.Text as Sundown
import           Text.Blaze.Html (preEscapedToHtml, Html)
import           System.FilePath.Posix.Lens

renderMarkdown :: Text -> Html
renderMarkdown = preEscapedToHtml . Sundown.renderHtml noExtensions noHtmlModes False Nothing

data FrontMatter = FrontMatter
  { title :: Text
  , author :: Text
  }

data Post = Post
  { frontMatter :: FrontMatter
  , body :: Html
  , inPath :: FilePath
  , outPath :: FilePath
  }

parsePostFromFile :: SourceName -> FilePath -> IO (Either ParseError Post)
parsePostFromFile sourceName path = do
  postContent <- T.readFile path
  let parsed = parse parsePost' sourceName postContent
  case parsed of
    Left err -> return (Left err)
    Right (frontMatter', body') -> return . Right $ Post frontMatter' body' path (getOutPath path)
  where getOutPath path = path & set _extension "html" & set (_directories . _head) "_posts"

anyText :: Parser Text
anyText = pack <$> many anyChar

parsePost' :: Parser (FrontMatter, Html)
parsePost' = do
  fm <- parseFrontMatter
  postFrontMatter <- FrontMatter <$> parseLookup "title" fm
                                 <*> parseLookup "author" fm
  markdown <- anyText
  return (postFrontMatter, renderMarkdown markdown)
  where parseLookup :: (Eq a, Show a) => a -> [(a,b)] -> Parser b
        parseLookup key table =
          let errorOut = fail $ "Front matter is missing required key: " ++ show key
          in maybe errorOut return (lookup key table)

parseFrontMatter :: Parser [(Text,Text)]
parseFrontMatter = between beginToken endToken . many . try $ do
  key   <- manyTill anyChar (char ':')
  spaces
  value <- manyTill anyChar (char '\n')
  return (pack key, pack value)
  where beginToken = string "---" *> spaces
        endToken   = spaces *> string "---"
