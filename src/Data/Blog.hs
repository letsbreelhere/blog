module Data.Blog where

import Data.Text (Text)

data Blog = Blog
  { title :: Text
  , author :: Text
  }
